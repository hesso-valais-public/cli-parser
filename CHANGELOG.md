# Changelog

## 0.6.0
- Several bugfixes
- Added C++ support
- Added support to execute script in Python 3.x 
