# CLI Parser

Fork of the [CLI Parser](https://sourceforge.net/projects/cliparser/) project located on [SourceForge](https://sourceforge.net). All credits go to [Henry Kwok](https://sourceforge.net/u/hkwok/profile/) the originator of this project.

# Links
- [Documentation](html/index.html)
